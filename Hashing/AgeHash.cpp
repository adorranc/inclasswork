/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
#include <map>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <string>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv)
{
	std::map<std::string, int> ageHashOrdered = {
		{"Matthew", 38},
		{"Alfred", 72},
		{"Rosco", 36},
		{"James", 35}
	};

	std::cout << "The Ordered hashes are:" << std::endl;
	for (auto iter = ageHashOrdered.begin(); iter != ageHashOrdered.end(); ++iter)
	{
		std::cout << iter->first << " " << iter->second << std::endl;
	}


	std::unordered_map<std::string, int> ageHashUnOrdered = {
		{"Matthew", 38},
		{"Alfred", 72},
		{"Rosco", 36},
		{"James", 35}
	};


	std::unordered_map<std::string, int>::iterator uiter;
	std::cout << "The Unordered hashes are:" << std::endl;
	for (uiter = ageHashUnOrdered.begin(); uiter != ageHashUnOrdered.end(); ++uiter)
	{
		std::cout << uiter->first << " " << uiter->second << std::endl;
	}

	std::cout << "The Ordered Example: Matthew, " << ageHashOrdered["Matthew"] << std::endl;
	std::cout << "The Unordered Example: James, " << ageHashUnOrdered["James"] << std::endl;
	return 0;
}
