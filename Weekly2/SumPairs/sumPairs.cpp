/**********************************************
* File: SumPairsWeekly.cpp
* Author: Aaron Dorrance
* Email: adorranc@nd.edu
* 
* This is the driver function for the solution to 
* the weekly coding report 2 problem 3.
* 
* Problem:
* 3) Design an algorthim to find all pairs of 
* integers witin an array which sum to a specified
* value.
*
* Compilation Instructions:
* make test
**********************************************/

#include<iostream>
#include "SeparateChaining.h"
#include<vector>
#include<algorithm>
using std::cout;
using std::endl;
using std::vector;
using std::sort;

#define rand(a) (rand() % (a))

/********************************************
* Function Name  : printParisUnsorted
* Pre-conditions : int iSum, const vector<int> & iNums
* Post-conditions: none
* 
* Prints the pairs of integers in the passed 
* vector that sum to iSum argument.
* This does not assume the array is sorted.
* This uses O(n) space and O(n) time. 
********************************************/
void printPairsUnsorted(int iSum, const vector<int> & viNums)
{
	// Insert the elements of vector into a Hash table for fast searching
	// Also find max and min to get limits of search
	int max = INT32_MIN;
	HashTable<int> htiNums(viNums.size());
	for (int e : viNums)
	{
		htiNums.insert(e);
		if (e > max)
			max = e;
	}

	// go though the array and check to see if its complemnt is in the array
	// take advantage of hash tables O(1) opperations for fast checking
	// remove numbers from hash table so don't print the same pair twice
	int iComplement;
	for (int i = 0; i < viNums.size(); ++i)
	{
		iComplement = iSum - viNums[i];
		if (htiNums.contains(iComplement) && htiNums.contains(viNums[i]))
		{
			cout << "Pair Found: " << viNums[i] << ", " << iComplement << endl;
			htiNums.remove(iComplement);
			htiNums.remove(viNums[i]);
		}
	}
}

/********************************************
* Function Name  : printParisSorted
* Pre-conditions : int iSum, const vector<int> & iNums
* Post-conditions: none
* 
* Prints the pairs of integers in the passed 
* vector that sum to iSum argument.
* This assumes the passed array is sorted.
* This uses O(1) space and O(n) time. 
********************************************/
void printPairsSorted(int iSum, const vector<int> & viNums)
{
	int iEnd = viNums.size() - 1;
	int iComplement;
	for (int iBeg = 0; iBeg < iEnd; ++iBeg) 
	{
		while (iBeg > 0 && viNums[iBeg] == viNums[iBeg - 1]) ++iBeg;
		
		iComplement = iSum - viNums[iBeg];
		while (viNums[iEnd] > iComplement) --iEnd;
	
		if (viNums[iEnd] == iComplement && iBeg <= iEnd)
		{
			cout << "Pair Found: " << viNums[iBeg] << ", " << viNums[iEnd] << endl;
		}
	}
}


/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* 
* Main driver function for Weekly coding 
* report 2, problem 3.
********************************************/
int main()
{
	srand(time(NULL));
	size_t iArraySize = 25;
	// create a vector of random integers
	vector<int> viNums;
	viNums.reserve(iArraySize);
	for (size_t iIndex = 0; iIndex < iArraySize; iIndex++)
	{
		viNums.push_back(rand(iArraySize)- iArraySize / 2);
	}

	// print array for reference
	cout << "Unsorted Array of Numbers" << endl;
	for (int e : viNums)
	{
		cout << e << " ";
	}
	cout << endl;

	// look for the paris which sum to a random number
	int iSum  = rand(iArraySize) - iArraySize / 2;
	cout << "Finding numbers which sum to: " << iSum << endl;
	printPairsUnsorted(iSum, viNums);

	// Sort the array to set the sorted method
	sort(viNums.begin(), viNums.end());
	cout << "\nSorted Array of Numbers" << endl;
	for (int e : viNums)
	{
		cout << e << " ";
	}
	cout << endl;

	// look for pairs which sum to a random number
	cout << "Finding numbers which sum to: " << iSum << endl;
	printPairsSorted(iSum, viNums);
	return 0;
}

